import '../../css/style.css';
import '../functions/scrollControl';
import '../functions/toggleDropdown';
import gifts from '../../data/gifts.json';

Parse.serverURL = 'https://parseapi.back4app.com';
Parse.initialize('tkgvQtcwKdCFSOOvKngHZBv0GARqt6EvsxF1vjBl', 'P2fXB4hvhxd4jyvRbxYOoYDtgEOPAEF9fsX3YMIy', 'IFU6vutlMLnW0QCFpnzU4YfiyYPQx07Zo27uyTBz');

const container = document.getElementById('giftContainer');
const modal = document.getElementById('modal');
const modalBg = document.getElementById('modalBg');
const modalClose = document.getElementById('modalClose');
const modalImg = document.getElementById('modalImg');
const modalTitle = document.getElementById('modalTitle');
const modalDescription = document.getElementById('modalDescription');
const reserve = document.getElementById('reserve');
const reserveForm = document.getElementById('reserve-form');
const result = document.getElementById('result');
const reserveButton = document.getElementById('reserveButton');

const giftMap = {};
let modalOpenGift = null;

async function generateTiles() {
    const stats = await getReservationStats();

    container.innerHTML = "";

    gifts.forEach(gift => {
        giftMap[gift.key] = gift;

        const available = stats[gift.key] === undefined || stats[gift.key] < gift.maxCount;
        const tile = document.createElement('div');
        if (available) {
            tile.className = "\"rounded-xl shadow m-2 p-4 w-40 h-40 md:m-4 md:w-48 md:h-48 hover:shadow-lg transition-all hover:cursor-pointer";
            tile.addEventListener('click', () => {
                openTile(gift.key);
            })
        } else {
            tile.className = "\"rounded-xl shadow m-2 p-4 w-40 h-40 md:m-4 md:w-48 md:h-48 opacity-30";
        }
        tile.innerHTML = "<div style=\"background-image: url('/assets/img/gifts/" + gift.key + ".jpg')\" class=\"bg-contain bg-no-repeat bg-center w-full h-full opacity-90\"></div>"
        container.appendChild(tile);
    })
}

async function getReservationStats() {
    return await Parse.Cloud.run("getGiftReservationStats");
}

function openTile(key) {
    modalOpenGift = giftMap[key];

    reserve.classList.remove('hidden');
    reserveForm.classList.add('hidden');
    result.classList.add('hidden');
    modalTitle.innerHTML = modalOpenGift.label;
    modalDescription.innerHTML = modalOpenGift.description;
    modalImg.style.backgroundImage = "url('/assets/img/gifts/" + modalOpenGift.key + ".jpg')";

    modal.classList.remove('hidden');
}

function closeModal() {
    modalOpenGift = null;
    modal.classList.add('hidden');
}

function showReserveForm() {
    reserve.classList.add('hidden');
    reserveForm.classList.remove('hidden');
}

async function createReservation() {

    const newReg = new Parse.Object('giftReservation');
    newReg.set('key', modalOpenGift.key);
    newReg.set('name', document.getElementById('name').value);
    newReg.set('contact', document.getElementById('contact').value)

    result.innerText = 'Odesílám...';
    result.classList.remove('hidden');
    reserve.classList.add('hidden');
    reserveForm.classList.add('hidden');

    try {
        const result = await newReg.save();
        // Access the Parse Object attributes using the .GET method
        console.log('Gift reservation created', result);
        document.getElementById('result').innerText = 'Úspěšně rezervováno, děkujeme!';
        generateTiles();
    } catch (error) {
        console.error('Error while creating gift reservation: ', error);
        document.getElementById('result').innerText = 'Něco se pokazilo. :-( Napište prosím Tonymu. smid@antoninsmid.cz ';
    }
};

modalBg.addEventListener('click', closeModal);
modalClose.addEventListener('click', closeModal);
reserve.addEventListener('click', showReserveForm);
reserveButton.addEventListener('click', createReservation);

generateTiles();
