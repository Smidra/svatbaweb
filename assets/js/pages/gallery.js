import '../../css/style.css';
import '../functions/scrollControl';
import '../functions/toggleDropdown';


const galleryItem = document.getElementsByClassName("gallery-item");
const lightBoxContainer = document.createElement("div");
const lightBoxContent = document.createElement("div");
const lightBoxImg = document.createElement("img");
const lightBoxPrev = document.createElement("div");
const lightBoxNext = document.createElement("div");
const lightBoxClose = document.createElement("div");


lightBoxContainer.classList.add("lightbox");
lightBoxContent.classList.add("lightbox-content");
lightBoxPrev.innerHTML = "<";
lightBoxPrev.classList.add("lightbox-prev");
lightBoxNext.innerHTML = ">";
lightBoxNext.classList.add("lightbox-next");
lightBoxClose.innerHTML = "X";
lightBoxClose.classList.add("lightbox-close");

lightBoxContainer.appendChild(lightBoxContent);
lightBoxContent.appendChild(lightBoxImg);
lightBoxContainer.appendChild(lightBoxPrev);
lightBoxContainer.appendChild(lightBoxNext);
lightBoxContainer.appendChild(lightBoxClose);

document.body.appendChild(lightBoxContainer);

let index = 1;

function showLightBox(n) {
    if (n > galleryItem.length) {
        index = 1;
    } else if (n < 1) {
        index = galleryItem.length;
    }
    let imageLocation = galleryItem[index-1].children[0].getAttribute("src");
    imageLocation = `${imageLocation.substring(0, imageLocation.length - 10)}.jpg`
    lightBoxImg.setAttribute("src", imageLocation);
}

function currentImage() {
    lightBoxContainer.style.display = "block";

    let imageIndex = parseInt(this.getAttribute("data-index"));
    showLightBox(index = imageIndex);
}
for (let i = 0; i < galleryItem.length; i++) {
    galleryItem[i].addEventListener("click", currentImage);
}

function slideImage(n) {
    showLightBox(index += n);
}
function prevImage() {
    slideImage(-1);
}
function nextImage() {
    slideImage(1);
}
lightBoxPrev.addEventListener("click", prevImage);
lightBoxNext.addEventListener("click", nextImage);

function closeLightBox() {
        lightBoxContainer.style.display = "none";
}
lightBoxClose.addEventListener("click", closeLightBox);

function keyDownHandler(event) {
    if(event.key === 'ArrowLeft') {
        prevImage();
    }
    if(event.key === 'ArrowRight') {
        nextImage();
    }
    if(event.key === 'Escape') {
        closeLightBox();
    }
}

document.addEventListener('keydown', keyDownHandler);
